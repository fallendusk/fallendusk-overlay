# fallendusk-overlay
A Gentoo ebuild repository (overlay) for my personal ebuilds

## How to use this repository?

### eselect-repository
The easiest way to use this repository is with [eselect-repository](https://wiki.gentoo.org/wiki/Eselect/Repository)
```
eselect repository add fallendusk-overlay git https://gitlab.com/fallendusk/fallendusk-overlay.git
emerge --sync
```

### Manual
For the [local repository](https://wiki.gentoo.org/wiki/Handbook:Parts/Portage/CustomTree#Defining_a_custom_repository) method, create a `/etc/portage/repos.conf/fallendusk-overlay.conf` file containing the following bit of text.

```
[fallendusk-overlay]
location = <repo-location>/fallendusk-overlay
sync-type = git
sync-uri = https://gitlab.com/fallendusk/fallendusk-overlay.git
auto-sync = Yes
```

Change `repo-location` to a path of your choosing and then run `emerge --sync`, Portage should now find and update the repository.