# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit gnome2-utils

DESCRIPTION="Lüv is the spiritual successor to Flattr, a flat but complex icon theme for freedesktop environments. "
HOMEPAGE="https://github.com/Nitrux/luv-icon-theme"

if [[ ${PV} == *99999999 ]];then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
else
	SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
fi
KEYWORDS="~x86 ~amd64"
LICENSE="CC-BY-SA-4.0"
SLOT="0"
IUSE="+wallpapers"

DEPEND=""
RDEPEND="
	${DEPEND}
	gnome-base/librsvg"

src_install() {
	dodoc README.md

	if use wallpapers ; then
		insinto /usr/share/backgrounds/Luv
			doins -r ./Wallpapers/*

			dosym "${ED%/}"/usr/share/backgrounds/Luv /usr/share/wallpapers/Luv

	fi

	insinto /usr/share/icons/Luv
		doins -r ./Luv/*
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
}

pkg_postrm() {
	gnome2_icon_cache_update
}